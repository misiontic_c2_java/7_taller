public class Cliente {
    /**
     * Atributos
     */
    private String nombre;
    private String apellido;
    private String cedula;
    private String celular;

    /**
     * Constructor
     */
    public Cliente(String nombre, String apellido, String cedula){
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
    }

    /**
     * Consultores
     */
    public String getNombre(){
        return this.nombre;
    }
    public String getApellido(){
        return this.apellido;
    }
    public String getCelular(){
        return this.celular;
    }

    /**
     * Modificadores
     */
    public void setCelular(String celular){
    this.celular = celular;
    }

}
