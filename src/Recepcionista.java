import java.util.ArrayList;
import java.util.List;

public class Recepcionista extends Empleado{

    /**
     * Atributos
     */
    private List<Cliente> clientes;

    /**
     * Constructor
     */
    public Recepcionista(String nombre, String apellido, String cedula){
        super(nombre, apellido, cedula);
        this.clientes = new ArrayList<Cliente>();
    }
    /**
     * Consultor
     */
    public Cliente getCliente(int pos){
        return this.clientes.get(pos);
    }

    /**
     * Métodos
     * (Acciones del obejto)
     */
    public void registrar_clientes(String nombre, String apellido, String cedula){
        // Aquí construir un objeto clientes
        Cliente objCliente = new Cliente(nombre, apellido, cedula);
        // Almacenar objeto en el arrayList
        this.clientes.add(objCliente);
    }
    public void registrar_vehiculo(){
        // Aquí construit un objeto de tipo vehiculo
    }
}
